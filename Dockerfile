FROM node:14.20.1
ENV NODE_ENV=production
RUN mkdir -p /app
WORKDIR /app
COPY . .
RUN npm install
CMD ["node","phone-book-app/backend/server.js"]
EXPOSE 3000
